<?php 
/*-------------------------------------------------------------------
    Template Name: Flexible Content
-------------------------------------------------------------------*/
?>
<?php get_header(); ?>

<?php get_template_part('template-parts/elements/main-header-homepage'); ?>

<main id="main-content">
<?php if( have_rows('article') ): ?>
	<article>
	<?php 
	/*----------------------------------------------------------------*\
	|
	| Insert page content which is most often handled via ACF Pro
	| and highly recommend the use of the flexiable content so
	|	we already placed that code here.
	|
	| https://www.advancedcustomfields.com/resources/flexible-content/
	|
	\*----------------------------------------------------------------*/
	?>
	<?php
		while ( have_rows('article') ) : the_row();
			if( get_row_layout() == 'editor' ):
				get_template_part( 'template-parts/sections/article/editor' );
			elseif( get_row_layout() == '2editor' ):
				get_template_part( 'template-parts/sections/article/editor-2-column' );
			elseif( get_row_layout() == '3editor' ):
				get_template_part( 'template-parts/sections/article/editor-3-column' );
			elseif( get_row_layout() == 'media+text' ):
				get_template_part( 'template-parts/sections/article/media-text' );
			elseif( get_row_layout() == 'cover' ):
				get_template_part( 'template-parts/sections/article/cover' );
			elseif( get_row_layout() == 'testimonials' ):
				get_template_part( 'template-parts/sections/article/testimonials' );
			elseif( get_row_layout() == 'callout' ):
				get_template_part( 'template-parts/sections/article/callout' );
			elseif( get_row_layout() == 'calculator' ):
				get_template_part( 'template-parts/sections/article/calculator' );
			endif;
		endwhile;
	?>
	</article>
<?php endif; ?>
</main>

<?php get_footer(); ?>