<?php 
/*-------------------------------------------------------------------
    Template Name: Gallery
-------------------------------------------------------------------*/
?>
<?php get_header(); ?>
<?php get_template_part('template-parts/elements/main-header-small'); ?>
<?php if( !empty(get_the_content()) ) { ?>
	<section class="default-contents">
		<?php get_template_part('template-parts/pages/content', 'default'); ?>
	</section>
<?php } ?>
<?php get_template_part('template-parts/elements/gallery'); ?>

<?php get_footer(); ?>