<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/posts/blog-title'); ?>

<?php if (!have_posts()) : ?>
<h3>Sorry, there are currently no posts.</h3>
<?php else: ?>

<?php 
$categories = get_categories( array(
	'orderby' => 'name',
	'order'   => 'ASC'
) );
if ( ! empty( $categories ) ) {
?>
<nav class="secondary-nav">
	<p>Sort by:</p>
	<ul class="secondnav">
		<li class="menu-item">
			<a href="<?php echo get_site_url(); ?>/blog/">All</a>
		</li>
		<?php 
			foreach( $categories as $category ) {
				$category_link = sprintf( 
					'<a href="%1$s" alt="%2$s">%3$s</a>',
					esc_url( get_category_link( $category->term_id ) ),
					esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
					esc_html( $category->name )
			);
		?>
		<li class="menu-item">
			<?php echo $category_link; ?>
		</li>
		<?php 
			}
		?>
	</ul>
	<div class="select-wrap">
		<select class="secondnav" onchange="location = value;">
			<option value="/blog/">All</option>
			<?php 
				foreach( $categories as $category ) {
					$category_link = sprintf( 
						'<a href="%1$s" alt="%2$s">%3$s</a>',
						esc_url( get_category_link( $category->term_id ) ),
						esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
						esc_html( $category->name )
				);
			?>
			<option value="/category/<?php echo sanitize_key( $category->name ); ?>"><?php echo $category_link; ?></option>
			<?php 
				}
			?>
		</select>
	</div>
</nav>
<?php
	} 
?>

<div class="default-contents">
	<section class="blog-feed post-feed feed">
		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part( 'template-parts/posts/previews/preview-post', get_post_type() ); ?>
		<?php endwhile; ?>
	</section>

<?php
$pagination = get_the_posts_pagination( array(
	'prev_text'	=> __( 'Previous page' ),
	'next_text'	=> __( 'Next page' ),
) );
if ( ! empty( $pagination ) ) {
	echo $pagination
?>
	<button class="btn button load-more">Load more</button>
<?php
}
?>

</div>

<?php endif; ?>

<?php get_footer(); ?>
