<?php 
/*-------------------------------------------------------------------
    Single Locations Page
-------------------------------------------------------------------*/
?>
<?php get_header(); ?>

<?php get_template_part('template-parts/elements/main-header-homepage'); ?>

<div class="promo"><h2><?php the_field('promo_text'); ?></h2></div>

<?php if( !empty(get_the_content()) ) { ?>
<section class="default-contents">
	<?php get_template_part('template-parts/pages/content', 'default'); ?>
</section>
<?php } ?>

<?php get_template_part('template-parts/elements/slider'); ?>

<?php get_template_part('template-parts/elements/icon-list'); ?>

<?php get_template_part('template-parts/elements/testimonials'); ?>

<?php get_template_part('template-parts/elements/contact'); ?>

<?php get_template_part('template-parts/elements/homepage-benefits'); ?>

<?php get_template_part('template-parts/elements/banner'); ?>

<?php get_footer(); ?>