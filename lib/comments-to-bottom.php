<?php 
/*---------------------------------------------------------------
  Move the comment field to the bottom of the comment form
---------------------------------------------------------------*/
function wpb_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
	}
	 
	add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );

function wpsites_change_comment_form_submit_label($arg) {
	$arg['label_submit'] = 'Post Your Comment';
	return $arg;
}
add_filter('comment_form_defaults', 'wpsites_change_comment_form_submit_label');