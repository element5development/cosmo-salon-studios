<?php

/*-----------------------------------------
		MENUS - www.wp-hasty.com
-----------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_nav' => __( 'Primary Navigation'),
		'mobile_nav' => __( 'Mobile Navigation'),
		'blog_cat_navigation' => __('Blog Categories'),
		'footer_nav_1' => __( 'Footer Navigation 1'),
		'footer_nav_2' => __( 'Footer Navigation 2'),
		'footer_nav_3' => __( 'Footer Navigation 3'),
	);
	register_nav_menus( $locations );

}
add_action( 'init', 'nav_creation' );