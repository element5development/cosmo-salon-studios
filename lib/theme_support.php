<?php

/*-----------------------------------------
  INCLUDE MAIN CSS AND JS
-----------------------------------------*/
function wp_main_assets() {
  wp_enqueue_style( 'style-name', get_stylesheet_uri() );

  wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), '1.1', 'all');

  wp_enqueue_script('vendors', get_template_directory_uri() . '/dist/scripts/vendors/vendors.js', array (), 1.1, true);

	wp_enqueue_script('main', get_template_directory_uri() . '/dist/scripts/master/main.js', array ( 'jquery' ), 1.1, true);
	
	wp_enqueue_script('maps','https://maps.googleapis.com/maps/api/js?key=AIzaSyAs0gC_we-R9MuxasYY87oY1QCSb1Zh56w');
}
add_action('wp_enqueue_scripts', 'wp_main_assets');


/*-----------------------------------------
  Enable Post Thumbnails
-----------------------------------------*/
add_theme_support('post-thumbnails');

/*-----------------------------------------
  Enable Post Formats
-----------------------------------------*/
add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

/*-----------------------------------------
  Enable HTML5 Markup Support
-----------------------------------------*/
add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

/*-----------------------------------------
  Use Main Stylesheet for Visual Editor
-----------------------------------------*/
add_editor_style('/assets/styles/main.css');