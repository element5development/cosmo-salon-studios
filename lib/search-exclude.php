<?php

/*---------------------------------------------------
  EXCLUDE INDIVIDUAL PAGES FROM THE INTERNAL SEARCH
---------------------------------------------------*/
function jp_search_filter( $query ) {
if ( $query->is_search && $query->is_main_query() ) {
$query->set( 'post__not_in', array( 176,231 ) );
}
}

add_action( 'pre_get_posts', 'jp_search_filter' );