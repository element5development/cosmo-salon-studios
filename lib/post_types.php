<?php

/*-----------------------------------------
	CUSTOM POST TYPES - www.wp-hasty.com
-----------------------------------------*/

// Register Custom Post Type Salon Expert
// Post Type Key: salonexpert
function create_salonexpert_cpt() {

	$labels = array(
		'name' => __( 'Salon Experts', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Salon Expert', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Salon Experts', 'textdomain' ),
		'name_admin_bar' => __( 'Salon Expert', 'textdomain' ),
		'archives' => __( 'Salon Expert Archives', 'textdomain' ),
		'attributes' => __( 'Salon Expert Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Salon Expert:', 'textdomain' ),
		'all_items' => __( 'All Salon Experts', 'textdomain' ),
		'add_new_item' => __( 'Add New Salon Expert', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Salon Expert', 'textdomain' ),
		'edit_item' => __( 'Edit Salon Expert', 'textdomain' ),
		'update_item' => __( 'Update Salon Expert', 'textdomain' ),
		'view_item' => __( 'View Salon Expert', 'textdomain' ),
		'view_items' => __( 'View Salon Experts', 'textdomain' ),
		'search_items' => __( 'Search Salon Expert', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Salon Expert', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Salon Expert', 'textdomain' ),
		'items_list' => __( 'Salon Experts list', 'textdomain' ),
		'items_list_navigation' => __( 'Salon Experts list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Salon Experts list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Salon Expert', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-admin-users',
		'supports' => array('title', 'editor', 'excerpt', 'custom-fields', ),
		'taxonomies' => array('type', ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		"rewrite" => array("slug" => "salon-experts"),
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'salonexpert', $args );

}
add_action( 'init', 'create_salonexpert_cpt', 0 );

// Register Taxonomy Type
// Taxonomy Key: type
function create_type_tax() {

	$labels = array(
		'name'              => _x( 'Type', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Type', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Types', 'textdomain' ),
		'all_items'         => __( 'All Types', 'textdomain' ),
		'parent_item'       => __( 'Parent Type', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Type:', 'textdomain' ),
		'edit_item'         => __( 'Edit Type', 'textdomain' ),
		'update_item'       => __( 'Update Type', 'textdomain' ),
		'add_new_item'      => __( 'Add New Type', 'textdomain' ),
		'new_item_name'     => __( 'New Type Name', 'textdomain' ),
		'menu_name'         => __( 'Type', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
	);
	register_taxonomy( 'type', array('salonexpert', ), $args );

}
add_action( 'init', 'create_type_tax' );

// Register Custom Post Type Location
function create_location_cpt() {

	$labels = array(
		'name' => _x( 'Locations', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Location', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Locations', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Location', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Location Archives', 'textdomain' ),
		'attributes' => __( 'Location Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Location:', 'textdomain' ),
		'all_items' => __( 'All Locations', 'textdomain' ),
		'add_new_item' => __( 'Add New Location', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Location', 'textdomain' ),
		'edit_item' => __( 'Edit Location', 'textdomain' ),
		'update_item' => __( 'Update Location', 'textdomain' ),
		'view_item' => __( 'View Location', 'textdomain' ),
		'view_items' => __( 'View Locations', 'textdomain' ),
		'search_items' => __( 'Search Location', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Location', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Location', 'textdomain' ),
		'items_list' => __( 'Locations list', 'textdomain' ),
		'items_list_navigation' => __( 'Locations list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Locations list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Location', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-location',
		'supports' => array('title', 'editor', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'page',
	);
	register_post_type( 'location', $args );

}
add_action( 'init', 'create_location_cpt', 0 );