<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<section class="main-header-small-section salon-expert-header" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.4)), url('<?= esc_url(home_url('/')); ?>wp-content/themes/cosmo-website/dist/images/salon-experts-header.jpg')">
	<div class="content">
    <h1>Cosmo Salon Experts</h1> 
		<h3>Here are the experts you can find at Cosmo Salon Studios</h3>
  </div>
</section>

<?php if (!have_posts()) : ?>
<h3>Sorry, there are currently no posts.</h3>
<?php else: ?>

<?php
$terms = get_terms( 'type' );
if ( ! empty( $terms ) ){
?>
<nav class="secondary-nav">
	<p>Sort by:</p>
	<div class="filters">
		<select class="service filter" data-filter-group="service">
			<option value="*">All Services</option>
			<?php foreach ( $terms as $term ) { ?>
			<option value=".<?php echo str_replace(' ', '-', esc_html__( $term->name )); ?>"><?php echo esc_html( $term->name ); ?></option>
			<?php } ?>
		</select>
		<select class="location filter" data-filter-group="location">
			<option value="*">All Locations</option>
			<option value=".Canton">Canton</option>
			<option value=".Clarkston">Clarkston</option>
			<option value=".Taylor">Taylor</option>
			<option value=".Troy">Troy</option>
		</select>
	</div>
</nav>
<?php
	} 
?>

<div class="default-contents">
	<section class="expert-feed post-feed feed">
		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part( 'template-parts/posts/previews/preview-post-expert' ); ?>
		<?php endwhile; ?>
	</section>
</div>

<?php endif; ?>

<?php get_footer(); ?>
