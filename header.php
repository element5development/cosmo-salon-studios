<?php 
/*-------------------------------------------------------------------

Main header functions

------------------------------------------------------------------*/
?>

<!DOCTYPE html>

<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-55ML9HP');</script>
	<!-- End Google Tag Manager -->
	<!-- CHOOZLE – Tracking Code -->
	<script type="text/javascript" src="//nexus.ensighten.com/choozle/15507/Bootstrap.js"></script>
	<!-- End CHOOZLE -->
	<!-- LOCALiQ – Tracking Code -->
	<script type="text/javascript" src="//cdn.rlets.com/capture_configs/6ed/919/626/c984c78b39f40847a8f34e5.js" async="async"></script>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<title><?php wp_title(''); ?></title>
	<?php wp_head(); ?>
	<!-- IF IS LOCATIONS PAGE -->
	<?php if ( is_page(562) || is_singular( 'location' ) ) : ?>
		<script type="text/javascript">
			// When the window has finished loading create our google map below
			google.maps.event.addDomListener(window, 'load', init);

			function init() {
				// Basic options for a simple Google Map
				// For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
				var mapOptions = {
						// How zoomed in you want the map to start at (always required)
						zoom: 9,

						// The latitude and longitude to center the map (always required)
						center: new google.maps.LatLng(42.3526897,-83.1692445), // New York

						// How you would like to style the map. 
						// This is where you would paste any style found on Snazzy Maps.
						styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#efc1c8"},{"visibility":"on"}]}]
				};

				// Get the HTML DOM element that will contain your map 
				// We are using a div with id="map" seen below in the <body>
				var mapElement = document.getElementById('map');

				// Create the Google Map using our element and options defined above
				var map = new google.maps.Map(mapElement, mapOptions);

				// Let's also add a marker while we're at it
				<?php if ( is_page(562) ) : ?>
					<?php
						$args = array(
							'post_type'      => 'location',
							'orderby' 			 => 'title',
							'order' 				 => 'ASC',
							'posts_per_page' => -1,
						);
						$locations = new WP_Query( $args );
					?>
					<?php if ( $locations->have_posts() ) : ?>
						<?php while ( $locations->have_posts() ) : $locations->the_post(); ?>
							<?php $location = get_field('map'); ?>
							var marker = new google.maps.Marker({
								position: new google.maps.LatLng(<?php echo $location['lat']; ?>,<?php echo $location['lng']; ?>),
								map: map,
								title: '<?php the_field('city'); ?>'
							});
						<?php endwhile; ?>
					<?php endif; wp_reset_postdata(); ?>
				<?php endif; ?>
				<?php if ( is_singular( 'location' ) ) : ?>
					<?php $location = get_field('map'); ?>
					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(<?php echo $location['lat']; ?>,<?php echo $location['lng']; ?>),
						map: map,
						title: '<?php the_field('city'); ?>'
					});
				<?php endif; ?>
			}
		</script>
	<?php endif; ?>
</head>

<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-55ML9HP"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php $hellobarlink = get_field('hello_bar', 'options'); ?>
	<?php if( get_field('hello_bar', 'options') ): ?>
	<div class="hello-bar">
		<a href="<?php echo $hellobarlink['url']; ?>" target="<?php echo $hellobarlink['target']; ?>"><?php echo $hellobarlink['title']; ?></a>
	</div>
	<?php endif; ?>

	<?php get_template_part('template-parts/navigation/primary'); ?>