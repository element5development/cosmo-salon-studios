var $ = jQuery;

$(document).ready(function () {
	/*-------------------------------------------------------------------
		Form input adding & removing classes
	------------------------------------------------------------------*/
	$('input:not([type=checkbox]):not([type=radio])').focus(function () {
		$(this).addClass('is-activated');
	});
	$('textarea').focus(function () {
		$(this).addClass('is-activated');
	});
	$('select').focus(function () {
		$(this).addClass('is-activated');
	});
	/*------------------------------------------------------------------
  	SEARCH ICON SWITCH
	------------------------------------------------------------------*/
	$(".header-search").click(function () {
		$(".search-icon").toggle();
		$(".close-icon").toggle();
	});
	/*------------------------------------------------------------------
  	PRIMARY NAV CONTROLS
	------------------------------------------------------------------*/
	$('.hamburger').on('click', function () {
		$('.hamburger').toggleClass('is-active');
		//menu dropdown
		$('.nav-dropdown').slideToggle(400);
	});
	$('.close').on('click', function () {
		$('.close').toggleClass('is-active');
		//menu dropdown
		$('.nav-dropdown').slideToggle(-400);
	});
	$('.header-search').on('click', function () {
		$('.search-icon-contain').toggleClass('open');
		$('.search-dropdown').slideToggle(400).toggleClass('active');
		$('.search-dropdown .search-wrap .search-contain form input').focus();
	});
	$('.header-phone').on('click', function () {
		$('.phone-dropdown').slideToggle(400).toggleClass('active');
	});
	$('main.main').on('click', function () {
		var activeDrop = $('.search-dropdown').hasClass('active');
		if (activeDrop) {
			$('.search-button .search-icon').toggle();
			$('.search-button .close-icon').toggle();
			$('.search-dropdown').slideUp(400).toggleClass('active');
		}
		var activeDrop2 = $('.phone-dropdown').hasClass('active');
		if (activeDrop2) {
			$('.phone-dropdown').slideUp(400).toggleClass('active');
		}
	});
	$(document).click(function (e) {
		if ($('.search-dropdown').hasClass('active') && $(e.target).closest('.primary-nav-container').length === 0) {
			// hide menu here
			$(".search-icon").toggle();
			$(".close-icon").toggle();
			$('.search-dropdown').slideUp(400).toggleClass('active');
		}
		if ($('.phone-dropdown').hasClass('active') && $(e.target).closest('.primary-nav-container').length === 0) {
			// hide menu here
			$('.phone-dropdown').slideUp(400).toggleClass('active');
		}
	});
	/*------------------------------------------------------------------
		Element5 Site Credit Appear
	------------------------------------------------------------------*/
	$('#element5-credit img').viewportChecker({
		classToAdd: 'visible',
		offset: 10,
		repeat: false,
		removeClassAfterAnimation: false,
	});
	/*------------------------------------------------------------------
		Gallery Lightbox
	------------------------------------------------------------------*/
	$('.gallery-card').featherlightGallery({
		previousIcon: '«',
		nextIcon: '»',
		galleryFadeIn: 200,
		openSpeed: 300
	});
	/*------------------------------------------------------------------
  	Secondary Nav Active Class
	------------------------------------------------------------------*/
	$('.secondary-nav ul li').on('click', function () {
		$('.secondary-nav ul li').removeClass('active');
		$(this).addClass('active').siblings().removeClass('active');
	});
	/*------------------------------------------------------------------
  	Isotope Gallery Filtering
	------------------------------------------------------------------*/
	// init Isotope
	var $galleryblock = $('.gallery-block').isotope({});
	var $categoryblock = $('.expert-feed').isotope({});
	var filterFns = {};
	// filter items on button click
	$('.secondnav').on('click', 'li', function () {
		var filterValue = $(this).attr('data-filter');
		$galleryblock.isotope({
			filter: filterValue
		});
	});
	$('.secondnav').on('change', function () {
		var filterValue = this.value;
		filterValue = filterFns[filterValue] || filterValue;
		$galleryblock.isotope({
			filter: filterValue
		});
	});

	var filters = {};

	$('.location, .service').on('change', function () {
		var filterGroup = $(this).attr('data-filter-group');
		var singleFilterValue = this.value;
		singleFilterValue = filterFns[singleFilterValue] || singleFilterValue;
		filters[filterGroup] = singleFilterValue;
		var filterValue = concatValues(filters);
		$categoryblock.isotope({
			filter: filterValue
		});
	});

	// flatten object by concatting values
	function concatValues(obj) {
		var value = '';
		for (var prop in obj) {
			value += obj[prop];
		}
		return value;
	}
	/*------------------------------------------------------------------
		Infinite Scroll on Blog
	------------------------------------------------------------------*/
	$('.feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.post-preview',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
		status: '.page-load-status',
	});
	/*------------------------------------------------------------------
		Gravity Forms Close Button
	------------------------------------------------------------------*/
	jQuery(document).bind('gform_confirmation_loaded', function (event, formId) {
		$('.button-x').click(function () {
			$('#overlay').addClass('remove');
			$('.calculator-block').addClass('minheight');
			$('.background-image').addClass('remove');
			$('.background-image-2').addClass('remove');
			$('.calculator-block h2').addClass('remove');
			$('.calculator-block h3').addClass('remove');
			$('.calculator-block .hidden').addClass('show');
		});
	});
	/*------------------------------------------------------------------
  	LOCATION SLIDER
  ------------------------------------------------------------------*/
	$('.slide-wrap').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		prevArrow: '<div class="prev"></div>',
		nextArrow: '<div class="next"></div>',
		responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
				}
			},
		]
	});
	/*------------------------------------------------------------------
  	TESTIMONIAL SLIDER
  ------------------------------------------------------------------*/
	$('.testimonial-slider').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M24.19 29.82a1.18 1.18 0 0 1 .39.91 1.21 1.21 0 0 1-.39.91 1.29 1.29 0 0 1-1.79 0L7.82 16.91a1.24 1.24 0 0 1 0-1.82L22.4.36a1.29 1.29 0 0 1 1.79 0 1.3 1.3 0 0 1 .39.93 1.17 1.17 0 0 1-.39.89L10.9 16z" data-name="Layer 1"/></svg>',
		nextArrow: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M7.81 2.18a1.18 1.18 0 0 1-.39-.91 1.21 1.21 0 0 1 .39-.91 1.29 1.29 0 0 1 1.79 0l14.58 14.73a1.24 1.24 0 0 1 0 1.82L9.6 31.64a1.29 1.29 0 0 1-1.79 0 1.3 1.3 0 0 1-.39-.93 1.17 1.17 0 0 1 .39-.89L21.1 16z" data-name="Layer 1"/></svg>',
	});
	/*------------------------------------------------------------------
  	PRECHECK VIRTUAL EVENT CHECKBOX
  ------------------------------------------------------------------*/
	$( "#input_5_9_1").prop('checked', true);

});