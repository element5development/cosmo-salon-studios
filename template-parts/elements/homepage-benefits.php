<?php if( have_rows('benefits') ): ?>
<section class="benefit-section">
	<div class="benefits-wrapper">
		<?php while ( have_rows('benefits') ) : the_row(); 
			$bottomImg = get_sub_field('bottom_image');
		?>
			<div class="benefit">
				<div class="content-wrap">
					<div class="background-image" style="background-image: url('<?php the_sub_field('background_image'); ?>')"></div>
					<div class="content">
						<div class="headline">
								<h3><?php the_sub_field('headline'); ?></h3>
						</div>
						<div class="main-content">
							<?php the_sub_field('content'); ?>
						</div>
						<?php if(get_sub_field('bottom_image')): ?>
							<div class="bottom-image">
								<img src="<?php echo $bottomImg['url']; ?>" alt="<?php echo $bottomImg['alt'] ?>" />
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
</section>
<?php endif;?>