<section class="homepage-main-content">
  <div class="main-content-left">
    <div class="headlines">
      <h2><?php the_field('main_content_left_headline'); ?></h2>
      <h3><?php the_field('main_content_left_sub_headline'); ?></h3>
    </div>
    <div class="content">
      <?php the_field('main_content_left_content'); ?>
    </div>
  </div>
  <div id="form-submitted" class="main-content-right">
    <div id="schedule-form"></div>
    <div class="headlines">
      <div class="content">
        <?php the_field('main_content_right_content'); ?>
      </div>
    </div>
  </div>
</section>