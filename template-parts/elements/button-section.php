<section class="button-block">
	<?php 
		$link = get_field('section_button');
		if( $link ): ?>    
			<a class="button" href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a>
			<?php get_template_part('template-parts/elements/testimonials'); ?>
	<?php endif; ?>
</section>