<?php 
/*----------------------------------------------------------------*\

	TESTIMONIALS SECTION

\*----------------------------------------------------------------*/
?>
<section class="testimonials <?php if( get_sub_field('use_red_version') ) : ?>is-red<?php endif; ?>">
	<div>
		<hr>
		<div class="testimonial-slider">
			<?php if ( have_rows('testimonials', 'options') ): ?>
				<?php while ( have_rows('testimonials', 'options') ) : the_row(); ?>

					<article class="testimonial">
						<blockquote>
							<p><?php the_sub_field('quote', 'options'); ?></p>
							<cite><?php the_sub_field('owner', 'options'); ?><span><?php the_sub_field('location', 'options'); ?></span></cite>
						</blockquote>
					</article>

				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>