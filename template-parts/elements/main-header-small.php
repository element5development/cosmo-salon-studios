<section class="main-header-small-section" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.4)), url('<?php the_field('main_header_background'); ?>')">
	<div class="content">
    <h1><?php the_field('main_header_headline'); ?></h1>
    <?php 
      $subheader = get_field('main_header_sub_headline');
      if( $subheader ): ?>    
				<h3><?php echo $subheader; ?></h3>
    <?php endif; ?>
  </div>
</section>