<?php
	$args = array(
		'post_type'      => 'location',
		'orderby' 			 => 'title',
		'order' 				 => 'ASC',
		'posts_per_page' => -1,
	);
	$locations = new WP_Query( $args );
?>
<?php if ( $locations->have_posts() ) : ?>
<section class="location-ctas">
	<?php while ( $locations->have_posts() ) : $locations->the_post(); ?>
		<div>
			<a href="<?php the_permalink(); ?>"></a>
			<h2><?php the_title(); ?></h2>
			<address>
				<?php the_field('address'); ?>
				<?php the_field('address_line_2'); ?><br/>
				<?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip'); ?><br/>
			</address>
			<div class="button">View Location</div>
		</div>
	<?php endwhile; ?>
</section>
<?php endif; wp_reset_postdata(); ?>