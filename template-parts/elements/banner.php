<?php if( get_field('banner_title') ): ?>
<section class="banner">
  <div>
		<h2><?php the_field('banner_title'); ?></h2>
		<h3><?php the_field('banner_description'); ?></h3>

		<?php $link = get_field('button_1'); ?>
		<?php if( get_field('button_1') ): ?>
		<a class="button" href="<?php echo $link['url']; ?>">
			<?php echo $link['title']; ?>
		</a>
		<?php endif; ?>

		<?php $link2 = get_field('button_2'); ?>
		<?php if( get_field('button_2') ): ?>
		<a class="button" href="<?php echo $link2['url']; ?>">
			<?php echo $link2['title']; ?>
		</a>
		<?php endif; ?>

  </div>
</section>
<?php endif; ?>