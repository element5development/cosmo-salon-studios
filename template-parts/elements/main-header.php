<section class="main-header-section" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.4)), url('<?php the_field('main_header_background'); ?>')">
	<div class="content">
    <h1><?php the_field('main_header_headline'); ?></h1>
    <h3><?php the_field('main_header_sub_headline'); ?></h3>
    <?php 
      $link = get_field('main_header_button');
      if( $link ): ?>    
        <a class="button smoothScroll" href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a>
    <?php endif; ?>
  </div>
</section>