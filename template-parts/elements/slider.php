<section class="slider">
  <div class="slide-wrap">
		<?php if( have_rows('image_repeater') ): ?>
			<?php while ( have_rows('image_repeater') ) : the_row(); ?>
				<?php $image = get_sub_field('image'); ?>
				<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php endwhile; ?>
		<?php endif; ?>
  </div>
</section>