<?php if( get_field('icon_repeater') ): ?>
<section class="icon-list">
  <div>
		<h2><?php the_field('icon_list_title'); ?></h2>
		<p><?php the_field('icon_list_description'); ?></p>
		<ul>
			<?php while ( have_rows('icon_repeater') ) : the_row(); ?>
				<?php $image = get_sub_field('icon'); ?>
				<li><img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" /><?php the_sub_field('icon_title'); ?></li>
			<?php endwhile; ?>
		</ul>
  </div>
</section>
<?php endif; ?>