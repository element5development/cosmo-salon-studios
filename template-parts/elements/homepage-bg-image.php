<section class="background-image-section" style="background-image: url('<?php the_field('background_section_background_image'); ?>')">
  <div class="content-wrap">
    <div class="headline-contain">
      <h2><?php the_field('background_section_headline'); ?></h2>
    </div>
    <div class="content">
      <div class="content-left">
        <?php the_field('background_section_left_content'); ?>
      </div>
      <div class="content-right">
        <?php the_field('background_section_right_content'); ?>
      </div>
    </div>
    <div class="button-contain">
    <?php 
      $link = get_field('background_section_button');
      if( $link ): ?>    
        <a class="button smoothScroll" href="#schedule-form"><?php echo $link['title']; ?></a>
    <?php endif; ?>
    </div>
  </div>
</section>