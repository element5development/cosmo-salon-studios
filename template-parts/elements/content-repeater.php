<section class="main-block">
	<?php if ( is_page( 'own-your-salon' ) ) {	?>
	<div class="headlines">
		<h2>Salon Amenities</h2>
	</div>
	<?php } else { } ?>
	<?php

	// check if the repeater field has rows of data
	if( have_rows('repeater') ):

		// loop through the rows of data
			while ( have_rows('repeater') ) : the_row();

	?>
	<div class="main-content-2">
		<div class="half-content">
			<h3><?php the_sub_field('repeater_headline'); ?></h3>
			<?php the_sub_field('repeater_content'); ?>
		</div>

		<?php $halfImg = get_sub_field('repeater_featured_image'); ?>

		<div class="half-image" style="background-image: url('<?php echo $halfImg['url']; ?>')">
		</div>
	</div>

	<?php

			endwhile;

	else :

			// no rows found

	endif;

	?>

</section>