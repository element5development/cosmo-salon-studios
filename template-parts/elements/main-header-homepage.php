<section class="main-header-homepage-section" style="background-image: linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.4)), url('<?php the_field('main_header_background'); ?>')">
	<div class="content">
		<h1>
			<?php the_field('main_header_headline'); ?>
		</h1>
		<h3>
			<?php the_field('main_header_sub_headline'); ?>
		</h3>
		<?php $link = get_field('main_header_button'); ?>
		<?php $link2 = get_field('main_header_button_2'); ?>
		<?php $link3 = get_field('main_header_button_3'); ?>
		<?php if ( is_front_page() || is_page_template( 'template-landing-page.php' ) ) : ?>
			<a class="button smoothScroll" href="#schedule-form">
				<?php echo $link['title']; ?>
			</a>
		<?php else: ?>
			<?php if ( get_field('main_header_button') ) : ?>
				<a class="button" href="<?php echo $link['url']; ?>">
					<?php echo $link['title']; ?>
				</a>
			<?php endif; ?>
			<?php if ( get_field('main_header_button_2') ) : ?>
				<a class="button" href="<?php echo $link2['url']; ?>">
					<?php echo $link2['title']; ?>
				</a>
			<?php endif; ?>
			<?php if ( get_field('main_header_button_3') ) : ?>
				<a class="button" href="<?php echo $link3['url']; ?>">
					<?php echo $link3['title']; ?>
				</a>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</section>