<section class="contact-section">
  <div class="contact-left">
    <div class="headline-contain">
      <h3>Cosmo Salon Studios</h3>
    </div>
    <div class="address-contain">
      <p><?php the_field('primary_address', 'option'); ?> <br/><?php the_field('primary_city', 'option'); ?>, <?php the_field('primary_state', 'option'); ?> <?php the_field('primary_zip', 'option'); ?></p>
    </div>
    <div class="phone-contain">

			<?php
				$phonenumber = get_field('expert_phone');
			?>
			<?php if( !empty($phonenumber) ): ?>
				<a href="tel:<?php $remove = array(" ","(",")",".","-"); echo str_replace($remove, "", $phonenumber); ?>">
					<svg width="29" height="29" version="1.1" xmlns="http://www.w3.org/2000/svg"><title>Page 1</title><path d="M22.137 17.897c-.566-.372-1.957-.76-3.057.6-1.102 1.359-2.461 3.625-7.607-1.521-5.146-5.145-2.88-6.505-1.52-7.605 1.36-1.102.97-2.493.6-3.059-.373-.567-2.769-4.256-3.19-4.872-.42-.615-.99-1.614-2.306-1.425C4.082.156.355 2.136.355 6.344s3.318 9.37 7.849 13.902c4.53 4.53 9.693 7.848 13.902 7.848 4.207 0 6.186-3.727 6.327-4.702.19-1.316-.809-1.885-1.424-2.306-.615-.42-4.305-2.816-4.872-3.189" fill="#7E0000" fill-rule="evenodd"/></svg>
					<span><?php echo $phonenumber; ?></span>
				</a>
			<?php else: ?>
				<a href="tel:<?php $remove = array(" ","(",")",".","-"); echo str_replace($remove, "", get_field('primary_phone', 'option')); ?>">
					<svg width="29" height="29" version="1.1" xmlns="http://www.w3.org/2000/svg"><title>Page 1</title><path d="M22.137 17.897c-.566-.372-1.957-.76-3.057.6-1.102 1.359-2.461 3.625-7.607-1.521-5.146-5.145-2.88-6.505-1.52-7.605 1.36-1.102.97-2.493.6-3.059-.373-.567-2.769-4.256-3.19-4.872-.42-.615-.99-1.614-2.306-1.425C4.082.156.355 2.136.355 6.344s3.318 9.37 7.849 13.902c4.53 4.53 9.693 7.848 13.902 7.848 4.207 0 6.186-3.727 6.327-4.702.19-1.316-.809-1.885-1.424-2.306-.615-.42-4.305-2.816-4.872-3.189" fill="#7E0000" fill-rule="evenodd"/></svg>
					<span><?php the_field('primary_phone', 'option'); ?></span>
				</a>
			<?php endif; ?>
			
    </div>
  </div>
  <div class="contact-right">
		<?php $location = get_field('google_map', 'option'); ?>
		<div class="acf-map">
			<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
		</div>
  </div>
</section>