
<?php if( get_field('gallery_repeater') ): ?>

	<?php while( has_sub_field('gallery_repeater') ): ?>

		<?php 

		// vars
		$select = get_sub_field_object('gallery_category');
		$value = get_sub_field('gallery_category');

		?>

	<?php endwhile; ?>

<?php endif; ?>

<nav class="secondary-nav">
	<p>Sort by:</p>
	<ul class="secondnav">
		<li class="menu-item active" data-filter="*">All</li>
		<?php foreach( $select['choices'] as $k => $v ): ?>
		<li class="menu-item" data-filter=".<?php echo $k; ?>"><?php echo $v; ?></li>
		<?php endforeach; ?>
	</ul>
	<div class="select-wrap">
		<select class="secondnav">
			<option value="*">All</option>
			<?php foreach( $select['choices'] as $k => $v ): ?>
			<option value=".<?php echo $k; ?>"><?php echo $v; ?></option>
			<?php endforeach; ?>
		</select>
	</div>
</nav>

<div class="gallery-block-outer">
	<div class="gallery-block">

	<?php if( have_rows('gallery_repeater') ): ?>

	<?php while ( have_rows('gallery_repeater') ) : the_row(); ?>

	<?php 

			$image = get_sub_field('gallery_image');
			$category = get_sub_field('gallery_category');

	?>

		<a href="<?php echo $image['url']; ?>" class="gallery-card <?php echo $category['value']; ?>">
			<div class="gallery-image" style="background-image: url(<?php echo $image['sizes']['medium']; ?>);"></div>
			<div class="gallery-category <?php echo $category['value']; ?>"><p><?php echo $category['label']; ?></p></div>
		</a>

	<?php endwhile; ?>

	<?php else : ?>

	<?php endif; ?>

		<div class="background-image"></div>
	</div>
</div>