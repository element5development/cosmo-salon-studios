<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<section class="copyright">
	<div>
		<p>©Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. <a href="<?php get_site_url(); ?>/privacy-policy/">Privacy Policy</a></p>
		<div id="element5-credit">
			<a target="_blank" href="https://element5digital.com">
				<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" alt="Crafted by Element5 Digital" />
			</a>
		</div>
	</div>
</section>