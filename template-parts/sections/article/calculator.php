<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	containing a single wysiwyg editor

\*----------------------------------------------------------------*/
?>
<section id="how-much" class="calculator-block">
	<h2><?php the_sub_field('calculator_headline'); ?></h2>
	<h3><?php the_sub_field('calculator_subheadline'); ?></h3>
	<?php the_sub_field('calculator'); ?>
	<p class="hidden">Not ready to decide yet? Check out our blog.</p>
	<a class="button hidden" href="/blog/">Blog</a>
	<div class="background-image"></div>
	<div class="background-image-2"></div>
</section>