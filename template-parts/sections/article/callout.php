<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	containing a callout

\*----------------------------------------------------------------*/
?>

<section class="callout">
	<div>
		<div>
			<?php the_sub_field('red_box_content'); ?>
		</div>
		<p><?php the_sub_field('disclaimer'); ?></p>
		<?php $button = get_sub_field('button'); ?>
		<a class="button" href="<?php echo $button['url']; ?>"><?php echo $button['title']; ?></a>
	</div>
</section>