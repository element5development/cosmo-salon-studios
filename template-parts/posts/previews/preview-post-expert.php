<?php
	$post_image = get_field('profile_picture');
	$terms = get_the_terms( $post->ID, 'type' );
?>

<?php	foreach ( $terms as $term ) : ?>
	<article class="expert-preview post-preview <?php echo str_replace(' ', '-', esc_html__( $term->name )); ?> <?php the_field('location'); ?>">
		<div class="post-head">
			<a href="<?php the_permalink(); ?>">
				<?php if ( ! empty( $post_image ) ) { ?>
					<div class="background-image" style="background-image: url(<?php echo $post_image['sizes']['medium']; ?>)"></div>
				<?php } else { ?>
					<div class="background-image" style="background-image: url(<?php echo get_template_directory_uri(); ?>/dist/images/no-profile-photo.jpg)"></div>
				<?php } ?>
				<div class="category-contain" style="background-color:<?php the_field('category_color', $term); ?>">
					<p><?php echo esc_html( $term->name ); ?></p>
				</div>
			</a>
		</div>
		<p><?php the_field('location'); ?>, MI</p>
		<a href="<?php the_permalink(); ?>">
			<h2><?php the_title(); ?></h2>
		</a>
		<p>Suite #<?php the_field('suite_number'); ?></p>
		<?php $phonenumber = get_field('expert_phone'); ?>
		<div class="buttons">
			<?php if ( $phonenumber ) : ?>
			<a class="button" href="tel:<?php $remove = array(" ","(",")",".","-"); echo str_replace($remove, "", $phonenumber); ?>"><?php echo $phonenumber; ?></a>
			<?php endif; ?>
			<a class="button" href="<?php the_permalink(); ?>">More Info</a>
		</div>
	</article>
<?php	endforeach; ?>