<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php
	$post_image = get_field('blog_header_image');
	$categories = get_the_category();
	if ( ! empty( $categories ) ) {
		foreach( $categories as $category ) {
?>

<article class="post-contain post-preview <?php echo esc_html( $category->name ); ?>">
	<a href="<?php the_permalink(); ?>">
		<div class="post-head">
		<?php if ( ! empty( $post_image ) ) { ?>
			<div class="background-image" style="background-image: url(<?php echo $post_image['sizes']['medium']; ?>)"></div>
		<?php } else { ?>
			<div class="background-image" style="background-image: url(<?= esc_url(home_url('/')); ?>wp-content/themes/cosmo-website/dist/images/default-<?php echo sanitize_title( $category->name ); ?>.png)"></div>
		<?php } ?>
				<div class="category-contain" style="background-color:<?php the_field('category_color', $category); ?>">
					<p style="color:<?php the_field('category_font_color', $category); ?>"><?php echo esc_html( $category->name ); ?></p>
				</div>
			<?php 
					}
				} 
			?>
		</div>
	</a>
	<div class="post-body">
		<p class="post-tags"><?php the_tags( '', ', ' ); ?></p>
		<a href="<?php the_permalink(); ?>">
			<h2><?php the_title(); ?></h2>
			<?php the_excerpt(); ?>
			<a class="button" href="<?php the_permalink(); ?>">Read The Post</a>
		</a>
	</div>
</article>