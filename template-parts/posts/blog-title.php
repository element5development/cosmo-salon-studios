<?php
	$args = array( 
		'posts_per_page' => '1',
		'post_status' => 'publish',
		'meta_key' => 'featured_post',
		'meta_value' => 'yes'
	);
	$recent_posts = wp_get_recent_posts( $args );
	$post_image = get_field('blog_header_image');
?>
<?php if( $recent_posts ): ?>
	<?php foreach( $recent_posts as $recent ): ?>
		<?php if ( is_home() ) : ?>
			<?php if ( get_field('blog_header_image') ) : ?>
				<section class="main-header-section" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.4)), url('<?php echo $post_image['url']; ?>')">
			<?php else : ?>
				<section class="main-header-section" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.4)), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-blog.jpg')">
			<?php endif; ?>
		<?php elseif ( is_category() ) : ?>
			<section class="main-header-section" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.4)), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/images/archive-category.jpg')">

		<?php elseif ( is_tag() ) : ?>
			<section class="main-header-section" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.4)), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/images/archive-tag.jpg')">

		<?php elseif ( is_author() ) : ?>
			<section class="main-header-section" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.4)), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/images/archive-author.jpg')">

		<?php else : ?>
			<section class="main-header-section" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.4)), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/images/archive-date.jpg')">

		<?php endif; ?>
			<div class="content">
				<?php if ( is_home() ) : ?>
					<h1><?php echo $recent["post_title"]; ?></h1>
					<div class="title-excerpt">
						<p><?php echo $recent["post_excerpt"]; ?></p>
					</div>
					<div class="title-cta-links">
						<a class="button smoothScroll" href="<?php echo get_permalink($recent["ID"]); ?>">Read The Post</a>
					</div>
				<?php else : ?>
					<h1>
						<?php the_archive_title(); ?>
					</h1>
				<?php endif; ?>
			</div>
		</section>
	<?php endforeach; ?>
	<?php wp_reset_query(); ?>
<?php else : ?>
<section class="main-header-section" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.4)), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-blog.jpg')">
	<div class="content">
		<h1>Blog</h1>
	</div>
</section>
<?php endif; ?>
<?php wp_reset_query(); ?>