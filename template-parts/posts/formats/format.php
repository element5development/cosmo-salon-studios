<?php 
/*-------------------------------------------------------------------

This is the default post format.
The other formats are SUPER basic so you can style them as you like.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>
<?php
	$post_image = get_field('blog_header_image');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">
	
	<?php if ( get_field('blog_header_image') ) : ?>
		<section class="main-header-section" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.4)), url('<?php echo $post_image['url']; ?>')">
	<?php else : ?>
		<section class="main-header-section" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.4)), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-blog.jpg')">
	<?php endif; ?>
		<div class="content">
			<div class="blog-subtitle">
				<?php the_category(); ?>
			</div>
			<h1 itemprop="headline" rel="bookmark"><?php the_title(); ?></h1>
		</div>
	</section>

  <section class="entry-content cf" itemprop="articleBody">
		<div class="main-block">
			<div class="post-tags"><?php the_tags( '', ', ' ); ?></div>
			<?php the_content(); ?>
			<div class="background-image"></div>
			<div class="background-image-2"></div>
			<div class="background-image-3"></div>
		</div>
		<section class="related-posts">
			<h2>Related Posts</h2>

			<?php $orig_post = $post;
			global $post;
			$categories = get_the_category($post->ID);
			if ($categories) {
			$category_ids = array();
			foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

			$args=array(
			'category__in' => $category_ids,
			'post__not_in' => array($post->ID),
			'posts_per_page'=> 3, // Number of related posts that will be shown.
			'caller_get_posts'=>1
			);

			$my_query = new wp_query( $args );
			if( $my_query->have_posts() ) {
			while( $my_query->have_posts() ) {
			$my_query->the_post();?>

			<?php
				$post_image = get_field('blog_header_image');
				$categories = get_the_category();
				if ( ! empty( $categories ) ) {
					foreach( $categories as $category ) {
			?>

			<article class="post-contain post-preview <?php echo esc_html( $category->name ); ?>">
				<a href="<?php the_permalink(); ?>">
					<div class="post-head">
					<?php 
								if ( ! empty( $post_image ) ) {
					?>
						<div class="background-image" style="background-image: url(<?php echo $post_image['url']; ?>)"></div>
					<?php
								} else {
					?>
						<div class="background-image" style="background-image: url(<?= esc_url(home_url('/')); ?>wp-content/themes/cosmo-website/dist/images/default-<?php echo sanitize_title( $category->name ); ?>.png)"></div>
					<?php
								} 
					?>
							<div class="category-contain" style="background-color:<?php the_field('category_color', $category); ?>">
								<p style="color:<?php the_field('category_font_color', $category); ?>"><?php echo esc_html( $category->name ); ?></p>
							</div>
						<?php 
								}
							} 
						?>
					</div>
				</a>
				<div class="post-body">
					<p class="post-tags"><?php the_tags( '', ', ' ); ?></p>
					<a href="<?php the_permalink(); ?>">
						<h2><?php the_title(); ?></h2>
						<?php the_excerpt(); ?>
						<a class="button" href="<?php the_permalink(); ?>">Read The Post</a>
					</a>
				</div>
			</article>

			<?
			}
			}
			else { ?>
				<p class="no-related">There are no related posts to display</p>
			<?
			}
			}
			$post = $orig_post;
			wp_reset_query(); ?>

		</section>
	</section>
	
</article>