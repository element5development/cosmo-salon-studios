<?php 
/*-------------------------------------------------------------------

Main header functions for Experts pages

------------------------------------------------------------------*/
?>

<!DOCTYPE html>

<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-55ML9HP');</script>
	<!-- End Google Tag Manager -->
	<!-- LOCALiQ – Tracking Code -->
	<script type="text/javascript" src="//cdn.rlets.com/capture_configs/6ed/919/626/c984c78b39f40847a8f34e5.js" async="async"></script>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<title><?php wp_title(''); ?></title>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-55ML9HP"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php $hellobarlink = get_field('hello_bar', 'options'); ?>
	<?php if( get_field('hello_bar', 'options') ): ?>
	<div class="hello-bar">
		<a href="<?php echo $hellobarlink['url']; ?>" target="<?php echo $hellobarlink['target']; ?>"><?php echo $hellobarlink['title']; ?></a>
	</div>
	<?php endif; ?>

	<?php get_template_part('template-parts/navigation/primary-experts'); ?>