<?php 
/*-------------------------------------------------------------------
    Template Name: Landing Page
-------------------------------------------------------------------*/
?>
<?php get_header(); ?>
<?php get_template_part('template-parts/elements/main-header-homepage'); ?>
<?php if( !empty(get_the_content()) ) { ?>
<section class="default-contents">
	<?php get_template_part('template-parts/pages/content', 'default'); ?>
</section>
<?php } ?>
<?php get_template_part('template-parts/elements/homepage-main-content'); ?>
<?php get_template_part('template-parts/elements/homepage-benefits'); ?>
<?php get_template_part('template-parts/elements/homepage-bg-image'); ?>
<?php get_footer(); ?>