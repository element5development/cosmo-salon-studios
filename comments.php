<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php if ( post_password_required() ) {
  return;
} ?>

<section class="comments-area">

  <?php
  // You can start editing here -- including this comment!
  if ( have_comments() ) : ?>
    <h2 class="comments-title">
      <?php $comments_number = get_comments_number();
      if ( '1' === $comments_number ) {
        /* translators: %s: post title */
        printf( _x( 'One Reply to &ldquo;%s&rdquo;', 'comments title', 'twentyseventeen' ), get_the_title() );
      } else {
        printf(
          /* translators: 1: number of comments, 2: post title */
          _nx(
            '%1$s Reply to &ldquo;%2$s&rdquo;',
            '%1$s Replies to &ldquo;%2$s&rdquo;',
            $comments_number,
            'comments title',
            'twentyseventeen'
          ),
          number_format_i18n( $comments_number ),
          get_the_title()
        );
      }
      ?>
    </h2>

    <ol class="comment-list">
      <?php
        wp_list_comments( array(
          'avatar_size' => 100,
          'style'       => 'ol',
          'short_ping'  => true,
          'reply_text'  => twentyseventeen_get_svg( array( 'icon' => 'mail-reply' ) ) . __( 'Reply', 'twentyseventeen' ),
        ) );
      ?>
    </ol>

    <?php the_comments_pagination( array(
      'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous', 'twentyseventeen' ) . '</span>',
      'next_text' => '<span class="screen-reader-text">' . __( 'Next', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
    ) );

  endif; // Check for have_comments().

  // If comments are closed and there are comments, let's leave a little note, shall we?
  if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
    <p class="no-comments"><?php _e( 'Comments are closed.', 'twentyseventeen' ); ?></p>
  <?php
	endif;
	
	$comment_args = array( 'title_reply' => __( 'Leave a Comment'),

		'fields' => apply_filters( 'comment_form_default_fields', array(
			'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Your Name' ) . '</label> ' .
			'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',   
			'email'  => '<p class="comment-form-email">' .
			'<label for="email">' . __( 'Your Email' ) . '</label> ' .
			'<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />'.'</p>',
			'url'    => '' ) ),
			'comment_field' => '<p>' .
			'<label for="comment">' . __( 'Your Comment' ) . '</label>' .
			'<textarea id="comment" name="comment" cols="45" rows="2" aria-required="true"></textarea>' .
			'</p>',
			'comment_notes_after' => '',
);

  comment_form($comment_args);
	?>
	
</section>
