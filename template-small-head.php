<?php 
/*-------------------------------------------------------------------
    Template Name: Small Header
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/main-header-small'); ?>
<?php if( !empty(get_the_content()) ) { ?>
	<section id="form-submitted" class="default-contents">
		<?php get_template_part('template-parts/pages/content', 'default'); ?>
		<div class="background-image"></div>
		<div class="background-image-2"></div>
		<div class="background-image-3"></div>
	</section>
<?php } ?>

<?php get_footer(); ?>