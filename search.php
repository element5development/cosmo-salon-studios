<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

	<section class="main-header-small-section" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/images/expert-profile-header.png')">
		<div class="content">
			<h1>Search: <?php echo get_search_query(); ?></h1>
		</div>
	</section>

	<section class="default-contents">
		<?php if (!have_posts()) : ?>
			<p>Sorry, no results were found</p>
			<?php get_search_form(); ?>
		<?php endif; ?>

		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part('template-parts/pages/content', 'search'); ?>
		<?php endwhile; ?>

	</section>

<?php get_footer(); ?>