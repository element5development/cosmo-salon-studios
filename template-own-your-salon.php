<?php 
/*-------------------------------------------------------------------
    Template Name: Own Your Salon
-------------------------------------------------------------------*/
?>
<?php get_header(); ?>
<?php get_template_part('template-parts/elements/main-header'); ?>
<?php if( !empty(get_the_content()) ) { ?>
	<section class="default-contents">
		<?php get_template_part('template-parts/pages/content', 'default'); ?>
	</section>
<?php } ?>
<?php get_template_part('template-parts/elements/content-repeater'); ?>
<?php get_template_part('template-parts/elements/button-section'); ?>
<?php get_template_part('template-parts/elements/calculator'); ?>
<?php get_footer(); ?>