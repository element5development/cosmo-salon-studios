<?php 
/*-------------------------------------------------------------------
    Template Name: Locations Page
-------------------------------------------------------------------*/
?>
<?php get_header(); ?>

<?php
	$args = array(
		'post_type'      => 'location',
		'orderby' 			 => 'title',
		'order' 				 => 'ASC',
		'posts_per_page' => -1,
	);
	$locations = new WP_Query( $args );
?>

<?php get_template_part('template-parts/elements/main-header-small'); ?>

	<div class="default-contents">
		<div id="map" class="acf-map"></div>
	</div>
	<div class="default-contents">
		<?php the_content(); ?>
	</div>
	<?php if ( $locations->have_posts() ) : ?>
	<div class="default-contents location-cards">
		<div class="locations-container">
			<?php while ( $locations->have_posts() ) : $locations->the_post(); ?>
				<div class="single-location">
					<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
					<address>
						<?php the_field('address'); ?>
						<?php the_field('address_line_2'); ?><br/>
						<?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip'); ?>
					</address>
					<?php $phone = preg_replace( '/[^0-9]/', '', get_field('phone') ); ?>
					<a href="tel:+1<?php echo $phone; ?>">
						<svg viewBox="0 0 29 29">
							<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<g id="Cosmo-Landing-Page" transform="translate(-29.000000, -23.000000)" fill="#7E0000">
									<g id="Page-1" transform="translate(29.000000, 22.000000)">
										<path d="M22.1374805,18.8972132 C21.5712404,18.5248717 20.1797604,18.1370745 19.0795967,19.4962381 C17.9780279,20.85587 16.6188643,23.1217671 11.473058,17.9759607 C6.32678333,12.8306228 8.5936171,11.4714592 9.95278065,10.3708271 C11.3124126,9.2692583 10.9236787,7.87824667 10.5522739,7.31200658 C10.1799324,6.74482979 7.78430124,3.05560466 7.36325092,2.43971904 C6.94266895,1.82523848 6.37268203,0.826239217 5.05707541,1.01545427 C4.0819622,1.15642885 0.35526875,3.13616156 0.35526875,7.34385466 C0.35526875,11.5515478 3.67308909,16.7146832 8.2039465,21.2455406 C12.7348039,25.776398 17.897471,29.09375 22.1056324,29.09375 C26.3133255,29.09375 28.2921215,25.3670565 28.4326278,24.3919433 C28.6232479,23.0758684 27.6242486,22.5068182 27.0092997,22.0862362 C26.3938824,21.6651859 22.7037206,19.2695547 22.1374805,18.8972132" id="Fill-1"></path>
									</g>
								</g>
							</g>
						</svg>
						<?php the_field('phone'); ?>
					</a>
					<a class="button" href="<?php the_permalink(); ?>">View Location</a>
				</div>
			<?php endwhile; wp_reset_postdata(); ?>
			<?php while ( have_rows('coming_soon') ) : the_row(); ?>
				<div class="single-location coming-soon">
					<h2><?php the_sub_field('title'); ?></h2>
					<?php if ( get_sub_field('address') ) : ?>
						<address>
							<?php the_sub_field('address'); ?>
							<?php the_sub_field('address_line_2'); ?><br/>
							<?php the_sub_field('city'); ?>, <?php the_sub_field('state'); ?> <?php the_sub_field('zip'); ?>
						</address>
					<?php endif; ?>
					<p>Call now with interest or questions.</p>
					<?php if ( get_sub_field('phone') ) : ?>
						<?php $phone = preg_replace( '/[^0-9]/', '', get_sub_field('phone') ); ?>
						<a class="button" href="tel:+1<?php echo $phone; ?>">
							<svg viewBox="0 0 29 29">
								<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<g id="Cosmo-Landing-Page" transform="translate(-29.000000, -23.000000)" fill="#ffffff">
										<g id="Page-1" transform="translate(29.000000, 22.000000)">
											<path d="M22.1374805,18.8972132 C21.5712404,18.5248717 20.1797604,18.1370745 19.0795967,19.4962381 C17.9780279,20.85587 16.6188643,23.1217671 11.473058,17.9759607 C6.32678333,12.8306228 8.5936171,11.4714592 9.95278065,10.3708271 C11.3124126,9.2692583 10.9236787,7.87824667 10.5522739,7.31200658 C10.1799324,6.74482979 7.78430124,3.05560466 7.36325092,2.43971904 C6.94266895,1.82523848 6.37268203,0.826239217 5.05707541,1.01545427 C4.0819622,1.15642885 0.35526875,3.13616156 0.35526875,7.34385466 C0.35526875,11.5515478 3.67308909,16.7146832 8.2039465,21.2455406 C12.7348039,25.776398 17.897471,29.09375 22.1056324,29.09375 C26.3133255,29.09375 28.2921215,25.3670565 28.4326278,24.3919433 C28.6232479,23.0758684 27.6242486,22.5068182 27.0092997,22.0862362 C26.3938824,21.6651859 22.7037206,19.2695547 22.1374805,18.8972132" id="Fill-1"></path>
										</g>
									</g>
								</g>
							</svg>
							<?php the_sub_field('phone'); ?>
						</a>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>